# OpenML dataset: shuttle-landing-control

https://www.openml.org/d/172

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

# Space Shuttle Autolanding Domain
 
NASA: Mr. Roger Burke's autolander design team

##### Past Usage: (several, it appears)
      Example: Michie,D. (1988).  The Fifth Generation's Unbridged Gap.
               In Rolf Herken (Ed.) The Universal Turing Machine: A
               Half-Century Survey, 466-489, Oxford University Press.
 
##### Relevant Information:
      This is a tiny database.  Michie reports that Burke's group used
      RULEMASTER to generate comprehendable rules for determining
      the conditions under which an autolanding would be preferable to
      manual control of the spacecraft.
 
##### Number of Instances:
15
 
##### Number of Attributes: 
7 (including the class attribute)
 
##### Attribute Information:
     1. Class: noauto, auto
        -- that is, advise using manual/automatic control
     2. STABILITY: stab, xstab
     3. ERROR: XL, LX, MM, SS
     4. SIGN: pp, nn
     5. WIND: head, tail
     6. MAGNITUDE: Low, Medium, Strong, OutOfRange
     7. VISIBILITY: yes, no
 
##### Missing Attribute Values:
    -- none
    -- but several "don't care" values: (denoted by "*")
          Attribute Number:   Number of Don't Care Values:
                         2:   2
                         3:   3
                         4:   8
                         5:   8
                         6:   5
                         7:   0
 
##### Class Distribution:
     1. Use noauto control: 6
     2. Use automatic control: 9%
 Information about the dataset\
 CLASSTYPE: nominal\
 CLASSINDEX: first

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/172) of an [OpenML dataset](https://www.openml.org/d/172). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/172/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/172/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/172/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

